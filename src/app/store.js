import { configureStore } from '@reduxjs/toolkit'
import todoListSlice from '../features/todoList/todoListSlice'

export default configureStore({
  reducer: {
    todoList: todoListSlice
  }
})