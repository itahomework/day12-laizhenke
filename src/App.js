import { useState } from 'react';
import './App.css';
import Header from './components/Header';
import List from './components/List';


function App() {

  const [todoList, setTodoList] = useState([])

  const addTodo = (todo) => {
    const newTodoList = [...todoList, todo]
    setTodoList(newTodoList)
  }

  return (
    <div className="todo-container">
      <div className="todo-wrap">
        <Header addTodo={addTodo} />
        <List todoList={todoList} />
      </div>
    </div>
  );
}

export default App;
