import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
  name: 'todoList',
  initialState: {
    value: []
  },
  reducers: {
    addTodo: (state, action) => {
      state.value = [...state.value, action.payload]
    },
    deleteTodo: (state, action) => {
      state.value = state.value.filter(todo => todo.id !== action.payload)
    },
    toggleTodoDone: (state, action) => {
      state.value.forEach(todo => {
        if (todo.id === action.payload) {
          todo.done = !todo.done
        }
      }
      )
    }
  }
})

export const { addTodo, deleteTodo, toggleTodoDone } = todoListSlice.actions
export default todoListSlice.reducer