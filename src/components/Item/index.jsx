import { useDispatch } from 'react-redux';
import { deleteTodo, toggleTodoDone} from '../../features/todoList/todoListSlice'
import './index.css';

const Item = (props) => {
  const dispatch = useDispatch()
  const { todo } = props

  const handleDelete = (id) => {
    return () => {
      dispatch(deleteTodo(id))
    }
  }

  return (
    <li>
      <p style={{textDecoration: todo.done ? 'line-through' : 'none'}} onClick={() => {dispatch(toggleTodoDone(todo.id))}}>{todo.title}</p>
      <button onClick={handleDelete(todo.id)} className="btn btn-danger">❌</button>
    </li>
  )
}
export default Item;