import { useDispatch } from 'react-redux'
import './index.css'
import { addTodo } from '../../features/todoList/todoListSlice'
import { nanoid } from 'nanoid'
const Header = () => {

  const dispatch = useDispatch()

  const handleKeyUp = (event) => {
    const {keyCode, target} = event
    if(keyCode !== 13)return
    if(target.value.trim() === ''){
      alert('输入不能为空')
      return
    }
    const todo = {id: nanoid(), title: target.value, done: false}
    dispatch(addTodo(todo))
    target.value = ''
  }
  return (
    <div className="todo-header">
      <input onKeyUp={handleKeyUp} type="text" placeholder="请输入你的任务名称，按回车键确认" />
    </div>
  )
}
export default Header;