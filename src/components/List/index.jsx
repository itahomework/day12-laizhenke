import { useSelector } from "react-redux";
import Item from "../Item";
import './index.css'

const List = () => {

  const todoList = useSelector(state => state.todoList.value)

  return(
    <ul className="todo-main">
      {
        todoList.map((todo, index) => {
          return <Item todo={todo} key={todo.id}/>
        })
      }
    </ul>
  )
}

export default List;