## Daily Report(07/25)

- O: learn Redux.
- R: benefit a lot.
- I: The use of Redux effectively solved the problem of yesterday's homework (worth passing on between various components). I have studied Vuex before, and their difference is that Vuex is a state management library specifically designed for Vue, while Redux is an independent state management library that can be used with various JavaScript frameworks and libraries, including React, Angular, and so on.
- D: I still need more practice to consolidate it.

